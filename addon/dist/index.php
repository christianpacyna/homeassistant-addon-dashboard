<?php

if(isset($_REQUEST['endpoint'])){
  $apiKey = getenv('SUPERVISOR_TOKEN');
  $apiRoot = 'http://supervisor/';

  $context = stream_context_create(
      array(
          'http' => array(
              'header'  =>
                  "Authorization: " . "Bearer " . $apiKey
          )
      )
  );
 
  $url = $apiRoot . $_GET['endpoint'];
  // Send the request to the API and return the result to the client
  $outData = file_get_contents($url, false, $context);
  echo($outData);
}

if ($_SERVER['REQUEST_METHOD'] == "GET" && !isset($_REQUEST['endpoint'])) {

  $config = file_get_contents('/config/homeassistant_dashboard/config.json');
  echo $config;

} else if ($_SERVER['REQUEST_METHOD'] == "POST") {

    #$file = $_SERVER['DOCUMENT_ROOT'] . '/config.json';
    #$file = $_SERVER['DOCUMENT_ROOT'] . '/config.json';

    $data = strval(file_get_contents("php://input"));

    $obj = json_decode($data);
    $json = json_encode($obj, JSON_PRETTY_PRINT);
    
    file_put_contents('/config/homeassistant_dashboard/config.json', $json);

    echo file_get_contents('/config/homeassistant_dashboard/config.json');
  }
