#!/bin/sh
DIR="/config/homeassistant_dashboard/"
if [ -d "$DIR" ]; then
  ### Take action if $DIR exists ###
  echo "Settingup rights"
  chmod -R 777 /config/homeassistant_dashboard/
else
  ###  Control will jump here if $DIR does NOT exists ###
  echo "Create Config Directory"
  mkdir /config/homeassistant_dashboard/
  chmod -R 777 /config/homeassistant_dashboard/
  echo "Directory created"
fi